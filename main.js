
var request = new XMLHttpRequest();
var method = "GET";
var url = "http://localhost:5000/reddit_feed";

function showData(data) {
    var list = document.getElementById("my_reddit_list");
    for (var i = 0; i < data.length; i++) {
        var article = data[i].data;
        var listItem = document.createElement("li");
        var link = document.createElement("a");
        link.href = article.url;
        var textNode = document
            .createTextNode(article.ups + " - " + article.title);

        link.appendChild(textNode);
        listItem.appendChild(link);
        list.appendChild(listItem);
    }
}

// method, url, async? <- asynchronous
request.open(method, url, true);

request.onreadystatechange = function () {
    if (request.readyState == XMLHttpRequest.DONE &&
        request.status == 200) {
        var jsonResponse = JSON.parse(request.responseText);
        console.log("Showing data...");
        showData(jsonResponse.data.children);
    }
};

request.send();
console.log("After sending the request...");

