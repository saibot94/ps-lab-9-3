from flask import Flask
from flask_cors import CORS
import json

app = Flask("My Flask Server")
CORS(app)

@app.route("/hello")
def hello():
    response = {"hello": "world"}
    return json.dumps(response)

import random

@app.route("/reddit_feed")
def get_reddit_data():
    children = []
    for i in range(0, 10):
        children.append({ "data": {
            "title": "Article " + str(i+1),
            "ups": random.randint(0, 20000)
        }})
    
    response = {
        "data": {
            "children": children
        }
    }

    return json.dumps(response)
    



app.run(debug=True)